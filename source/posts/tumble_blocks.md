---
filename: tumble_blocks
title: Tumble Blocks
subtitle: Spatial puzzle game with tumbling blocks
date: 2018 06 01
tags: [Team, Game, Unity, C#, Scripting, Math, Design]
completion: Likely
mainimage: tumbleblock3.png
---

*Tumble Blocks* is a 3D spatial puzzle game in which the player rotates and combines blocks to solve levels. I worked on prototyping this project and gained valuable experience in both programming skills and technical problem solving.

![][tb1]


The main challenge of *Tumble Blocks* was designing rules to accommodate, and simplify the complex relationship of rotating transforms. Given the
Each move in the 3D environment requires rotating any combination of blocks, simulating the physics of 3D rotational movement and collision of blocks and accommodation of additional rules involving color and breaking and combining blocks under certain situations.

![][tb2]

**WORK IN PROGRESS...**


[tb1]: /images/tumbleblock1.png#medium#left "tumbleblocks1"
	loading="lazy" option="small right"

[tb2]: /images/tumbleblock2.png#medium#right "tumbleblocks2"
	loading="lazy" option="small"

[tb3]: /images/tumbleblock3.png#small#left#small "tumbleblocks3"
	loading="lazy" option="small"
