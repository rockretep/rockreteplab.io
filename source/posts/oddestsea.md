---
filename: oddestsea
title: OddestSea
subtitle: Published, team developed indie game about sailing in a dark, eerie sea
date: 2020 05 01
tags: [Team, Game, Unity, C#, Scripting, Design, AI, Shaders]
confidence: Likely
mainimage: oddestsea2.png
---

*Official website: <https://www.oddestsea.com>*

*OddestSea* is a game about sailing in a dark, eerie sea, evading monsters, and restoring light to the mysterious waters.

![][os1]

I've worked on a variety of tasks in the making of this game:

- Programming
	- Gameplay mechanics
	- AI system and design
	- UI system
	- Physics implementations
- Art
	- Visual effects (in-engine)
	- Shaders (GLSL, Cg, HLSL)
	- Scene lighting
	- 3D modeling, rigging, and animation assistance
- Project Management
	- Repository management
	- Defining technical constraints
	- Project architecture and organization
	- Technical review and assistance for team

I've always wanted to make a game about sailing. I started this project in summer of 2018 for my Game Design BS capstone course at Indiana University. With the help of many talented artists and others, it release on Steam and itch.io in March 2020.

This venture into game development has been my most involved yet, and also ridden with mistakes.

**WORK IN PROGRESS...**

[os1]: /images/oddestsea1.png "oddestsea1"
	loading="lazy" style="filter:brightness(1.5)" option="small right"

[os2]: /images/oddestsea2.png "oddestsea2"
	loading="lazy"
